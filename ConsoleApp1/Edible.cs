﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Les_9
{
    class Edible : Product
    {
        public override Product Add(Product product)
        {
            Edible edible = new Edible(_name, _prise, _producer);
            return edible;
        }

        public Edible(string name, double prise, string produser) : base(name, prise, produser)
        {
            this._food = 1;
        }

        public override string ToString()
        {
            return $"Товар {_name} Цена {_prise} Поставщик {_producer}";
        }

        public override bool Equals(object obj)
        {
            Product product = (Product)obj;
            if (Name == product.Name && Prise == product.Prise)
            {
                return true;
            }

            else return false;
        }

    }
}
