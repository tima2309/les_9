﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Les_9
{
    class NotEdible : Product
    {
        public override Product Add(Product product)
        {
            NotEdible notedible = new NotEdible(_name, _prise, _producer);
            return notedible;
        }

        public NotEdible(string name, double prise, string produser) : base(name, prise, produser)
        {
            this._food = 2;
        }

        public override string ToString()
        {
            return $"Товар {_name} Цена {_prise} Поставщик {_producer}";
        }

        public override bool Equals(object obj)
        {
            Product product = (Product)obj;
            if (Name == product.Name && Prise == product.Prise)
            {
                return true;
            }

            else return false;
        }
    }
}
