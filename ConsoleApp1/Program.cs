﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Les_9
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Market market = new Market();

            while (true)
            {
                Console.Clear();
                Console.WriteLine("1 Добавить товар / Удалить товар");
                Console.WriteLine("Все товары");
                Console.WriteLine("Сортировка товаров по цене");
                Console.WriteLine("Список продовольственных товаров / не продовольственных товаров");
                Console.WriteLine("Сортировка по сторанам поставщикам");
                Console.WriteLine("Выход");

                int _choice = Convert.ToInt32(Console.ReadLine());

                if (_choice == 1)
                {
                    Console.Clear();
                    Console.WriteLine("1. Добавить товар");
                    Console.WriteLine("2. Удалить товар");

                    int _choice_Add_Del = Convert.ToInt32(Console.ReadLine());

                    if (_choice_Add_Del == 1)
                    {
                        Console.Clear();
                        Console.WriteLine("Тип товара");
                        Console.WriteLine("Продовольственный");
                        Console.WriteLine("Не продовольственный");

                        int _choice_edible_or_not = Convert.ToInt32(Console.ReadLine());

                        if (_choice_edible_or_not == 1)
                        {
                            market.AddEdible();
                        }
                        if (_choice_edible_or_not == 2)
                        {
                            market.AddNotEdible();
                        }
                        Console.WriteLine("Товар добавлен");

                    }

                    if (_choice_Add_Del == 2)
                    {
                        Console.Clear();
                        Console.WriteLine("Наименование товара");
                        market.Remove();
                        Console.WriteLine("Товар удален");
                    }

                    Console.WriteLine("Хотите продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "да")
                    {
                        continue;
                    }
                    break;

                }

                if (_choice == 2)
                {
                    Console.Clear();
                    market.Show();

                    Console.WriteLine("Хотите продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "да")
                    {
                        continue;
                    }
                    break;

                }

                if (_choice == 3)
                {
                    Console.Clear();
                    market.Sort_Prise();

                    Console.WriteLine("Хотите продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (_choice == 4)
                {
                    Console.Clear();
                    Console.WriteLine("1 Показать продовольственные товары");
                    Console.WriteLine("2 Показать не продовольственные товары");
                    market.Show_Class();

                    Console.WriteLine("Хотите продолжить");
                    string answer = Console.ReadLine();
                    if (answer == "да")
                    {
                        continue;
                    }
                    break;

                }

                if (_choice == 5)
                {
                    Console.Clear();
                    Console.WriteLine("Сортировка товаров по импортеру");
                    market.Sort_Producer();

                    Console.WriteLine("Хотите продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "да")
                    {
                        continue;
                    }
                    break;

                }

                if (_choice == 6)
                {
                    break;
                }
            }


        }

    }
}

   
  