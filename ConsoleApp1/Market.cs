﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Les_9
{
    class Market
    {
        List<Product> products = new List<Product>();
        private static string _marketname = "Евроопт";
        private static string _adressname = "Ул. Корженевского 20";

        public static string MarketName = _marketname;
        public static string AdressName = _adressname;

        public void AddEdible()
        {
            Console.WriteLine("Название товара");
            string name = Console.ReadLine();

            Console.WriteLine("Введите цену товара");
            double prise = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите поставщика");
            string producer = Console.ReadLine();

            Product product = new Edible(name, prise, producer);
            products.Add(product);
        }

        public void AddNotEdible()
        {
            Console.WriteLine("Название товара");
            string name = Console.ReadLine();

            Console.WriteLine("Введите цену товара");
            double prise = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите поставщика");
            string producer = Console.ReadLine();

            Product product = new NotEdible(name, prise, producer);
            products.Add(product);
        }

        public void Remove()
        {
            string _name = Console.ReadLine();
            for (int i = 0; i < products.Count; i++);
            {
                if (_name == products[i].Name);
                {
                    products.Remove(products[i]);
                }
            }
        }

        public void Show()
        {
            foreach (Product product in products)
            {
                Console.WriteLine(product);
            }
        }

        public void Sort_Prise()
        {
            var sortedProduct = products.OrderBy(p => p.Prise);
            foreach (var p in sortedProduct)
                Console.WriteLine($"{p.Name} - {p.Prise}");
        }

        public void Show_Class()
        {
            int _choice = Convert.ToInt32(Console.ReadLine());
            if (_choice == 1)
            {
                foreach (Product product in products)
                {
                    if (product.Food == 1)
                        Console.WriteLine(product);
                }
            }

            else
            {
                foreach (Product product in products)
                {
                    if (product.Food == 2)
                        Console.WriteLine(product);
                }
            }
        }

        public void Sort_Producer()
        {
            var sortedProduct = products.OrderBy(p => p.Producer);
            foreach (var p in sortedProduct)
                Console.WriteLine($"{p.Name} - {p.Producer}");
        }

    }

  