﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Les_9
{
    abstract class Product
    {
        protected string _name;
        protected double _prise;
        protected string _producer;
        protected int _food;
        public string Name => _name;
        public double Prise => _prise;
        public string Producer => _producer;
        public int Food => _food;
        public Product(string name, double prise, string producer)
        {
            this._name = name;
            this._prise = prise;
            this._producer = producer;
        }

        public abstract Product Add(Product product);

    }
}
